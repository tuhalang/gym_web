import {
    BOOKMARK_UNIT,
    BOOKMARK_UNIT_SUCCESS,
    CANCEL_SERVICE,
    GET_OTP, LATEST_LESSONS, LATEST_LESSONS_SUCCESS,
    LIST_BOOKMARK_UNIT,
    LIST_BOOKMARK_UNIT_SUCCESS,
    LIST_CATEGORIES,
    LIST_CATEGORIES_SUCCESS,
    LIST_LESSON_CATEGORIES,
    LIST_LESSON_CATEGORIES_SUCCESS,
    LIST_LESSON_UNIT,
    LIST_LESSON_UNIT_SUCCESS,
    LIST_TRAINER,
    LIST_TRAINER_SUCCESS,
    LOGIN,
    RECENT_LESSONS, RECENT_LESSONS_SUCCESS,
    REFRESH_TOKEN,
    REGISTER_SERVICE,
    SHOW_DRAWER,
    SHOW_OTP,
    SHOW_SUBSCRIBER,
    SHOW_VIDEO,
    VISIT_LESSON,
    CHECK_REGISTER,
    SHOW_RENEW,
    RENEW_SERVICE
} from "./actions_type";
import {GAME_CODE} from "../../utils/constant";

export const showDrawer = () => {
    return {
        type: SHOW_DRAWER
    }
}

export const showSubscriber = (visible) => {
    return {
        type: SHOW_SUBSCRIBER,
        visible,
    }
}

export const showRenew = (visible) => {
    return {
        type: SHOW_RENEW,
        visible
    }
}

export const showVideo = (path) => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    return {
        type: SHOW_VIDEO,
        path,
    }
}

export const showOtp = (visible) => {
    return {
        type: SHOW_OTP,
        visible,
    }
}

export const listCategories = ({page, size, language, parentId, trainerId, callback}) => {
    return {
        type: LIST_CATEGORIES,
        payload: {
            page, size, language, parentId, trainerId
        },
        callback,
    }
}

export const listCategoriesSuccess = (data) => {
    return {
        type: LIST_CATEGORIES_SUCCESS,
        payload: {...data},
    }
}

export const listLessonByCategories = ({page, size, language, categoryId, parentId, trainerId, callback}) => {
    return {
        type: LIST_LESSON_CATEGORIES,
        payload: {
            page: page,
            size: size,
            language: language,
            categoryId: categoryId,
            trainerId: trainerId ,
        },
        payloadParent: {
            page: page,
            size: size,
            language: language,
            categoryId: parentId,
            trainerId: trainerId,
        },
        callback,
    }
}

export const listLessonByCategoriesSuccess = (data) => {
    return {
        type: LIST_LESSON_CATEGORIES_SUCCESS,
        payload: {...data},
    }
}


export const listLessonUnit = ({lessonId, language, callback}) => {
    return {
        type: LIST_LESSON_UNIT,
        payload: {
            lessonId, language,
        },
        callback,
    }
}

export const listLessonUnitSuccess = (data) => {
    return {
        type: LIST_LESSON_UNIT_SUCCESS,
        payload: {...data},
    }
}

export const bookmarkUnit = ({unitId, callback}) => {
    return {
        type: BOOKMARK_UNIT,
        payload: {
            unitId,
        },
        callback,
    }
}

export const bookmarkUnitSuccess = (data) => {
    return {
        type: BOOKMARK_UNIT_SUCCESS,
        payload: {...data},
    }
}

export const listBookmarkUnit = ({page, size, language, callback}) => {
    return {
        type: LIST_BOOKMARK_UNIT,
        payload: {
            page, size, language,
        },
        callback,
    }
}

export const listBookmarkUnitSuccess = (data) => {
    return {
        type: LIST_BOOKMARK_UNIT_SUCCESS,
        payload: {...data},
    }
}

export const listTrainer = ({name, page, size, language, trainerId = "", callback}) => {
    return {
        type: LIST_TRAINER,
        payload: {
            page, size, language, name, trainerId,
        },
        callback,
    }
}

export const listTrainerSuccess = (data) => {
    return {
        type: LIST_TRAINER_SUCCESS,
        payload: {...data},
    }
}

export const login = ({isdn, otp, gameCode = GAME_CODE, language, callback, callbackSuccess}) => {
    return {
        type: LOGIN,
        payload: {
            isdn,
            otp,
            gameCode,
            language,
        },
        callback,
        callbackSuccess,
    }
}

export const otp = ({isdn, gameCode = GAME_CODE, language, callback, callbackSuccess}) => {
    return {
        type: GET_OTP,
        payload: {
            isdn,
            gameCode,
            language,
        },
        callbackSuccess,
        callback,
    }
}

export const refreshToken = ({isdn, refreshToken, gameCode = GAME_CODE, language, callback}) => {
    return {
        type: REFRESH_TOKEN,
        payload: {
            isdn,
            refreshToken,
            gameCode,
            language,
        },
        callback,
    }
}

export const renew = ({
    cost, isdn, gameCode, otpType, transId, otp, subService, language, callback, callbackSuccess
}) => {
    return {
        type: RENEW_SERVICE,
        payload: {
            cost,
            isdn: isdn,
            gameCode,
            otpType,
            transId,
            otp,
            subService,
            language,
        },
        callback,
        callbackSuccess, 
    }
}

export const register = ({
                             cost,
                             isdn,
                             gameCode = GAME_CODE,
                             otpType,
                             otp,
                             transId,
                             language,
                             subService,
                             callback,
                             callbackSuccess
                         }) => {
    return {
        type: REGISTER_SERVICE,
        payload: {
            cost,
            isdn: isdn,
            gameCode,
            otpType,
            transId,
            otp,
            subService,
            language,
        },
        callback,
        callbackSuccess,
    }
}

export const cancelService = ({
                                  isdn,
                                  gameCode = GAME_CODE,
                                  otpType,
                                  transid,
                                  otp,
                                  language,
                                  callback,
                                  callbackSuccess
                              }) => {
    return {
        type: CANCEL_SERVICE,
        payload: {
            isdn,
            gameCode,
            otpType,
            transid,
            language,
            otp,
        },
        callback,
        callbackSuccess,
    }
}

export const visitLesson = ({lessonId, callback}) => {
    return {
        type: VISIT_LESSON,
        payload: {
            lessonId,
        },
        callback,
    }
}

export const recentLessons = ({callback, callbackSuccess}) => {
    return {
        type: RECENT_LESSONS,
        payload: {},
        callback,
        callbackSuccess,
    }
}

export const recentLessonsSuccess = (data) => {
    return {
        type: RECENT_LESSONS_SUCCESS,
        payload: [...data],
    }
}

export const latestLessons = ({callback, callbackSuccess}) => {
    return {
        type: LATEST_LESSONS,
        payload: {},
        callback,
        callbackSuccess,
    }
}

export const latestLessonsSuccess = (data) => {
    return {
        type: LATEST_LESSONS_SUCCESS,
        payload: [...data],
    }
}

export const checkRegister = (isdn, token, callback) => {
    return {
        type: CHECK_REGISTER,
        payload: {
            isdn,
            token,
            gameCode: GAME_CODE
        },
        callback
    }
}