import _ from "lodash";
import {call, put, takeLatest} from "redux-saga/effects";
import {
    latestLessonsSuccess,
    listBookmarkUnitSuccess,
    listCategoriesSuccess,
    listLessonByCategoriesSuccess,
    listLessonUnitSuccess,
    listTrainerSuccess, recentLessonsSuccess
} from "../actions";
import {message} from "antd";
import getFactory from "../../api"
import {
    BOOKMARK_UNIT, CANCEL_SERVICE, CHECK_REGISTER, GET_OTP, LATEST_LESSONS,
    LIST_BOOKMARK_UNIT,
    LIST_CATEGORIES,
    LIST_LESSON_CATEGORIES,
    LIST_LESSON_UNIT,
    LIST_TRAINER, LOGIN, RECENT_LESSONS, REFRESH_TOKEN, REGISTER_SERVICE, RENEW_SERVICE, VISIT_LESSON
} from "../actions/actions_type";

const api = getFactory("common")

function* listCategories(action) {
    console.log("======== listCategories ========", action)
    try {
        const res = yield call((payload) => api.listCategories(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(listCategoriesSuccess(result.wsResponse))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listLessonByCategories(action) {
    console.log("======== listLessonByCategories ========", action)
    try {
        const res = yield call((payload) => api.listLessonByCategory(payload), action.payload)
        const res1 = yield call((payload) => api.listLessonByCategory(payload), action.payloadParent)
        if (res.errorCode !== "S200" || res1.errorCode !== "S200") {
            message.error(res.errorMessage)
        } else {
            if (res.result.errorCode == "0" && res1.result.errorCode == "0") {
                let data1 = res.result.wsResponse.lessons
                let data2 = res1.result.wsResponse.lessons
                res.result.wsResponse.lessons = [...new Set(data1.concat(data2))]
                console.log( [...new Set(data1.concat(data2))])
                yield put(listLessonByCategoriesSuccess(res.result.wsResponse))
            } else {
                message.error(res.result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listLessonUnits(action) {
    console.log("======== listLessonUnits ========", action)
    try {
        const res = yield call((payload) => api.listLessonUnits(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(listLessonUnitSuccess(result.wsResponse))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listTrainers(action) {
    console.log("======== listTrainers ========", action)
    try {
        const res = yield call((payload) => api.listTrainers(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(listTrainerSuccess(result.wsResponse))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listBookmarkUnit(action) {
    console.log("======== listBookmarkUnit ========", action)
    try {
        const res = yield call((payload) => api.listBookmarkUnit(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(listBookmarkUnitSuccess(result.wsResponse))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* bookmarkUnit(action) {
    console.log("======== bookmarkUnit ========", action)
    try {
        const res = yield call((payload) => api.bookmarkUnit(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                message.success(result.message)
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* login(action) {
    console.log("======== login ========", action)
    try {
        const res = yield call((payload) => api.login(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res

        console.log(errorCode, result, errorMessage)

        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            // yield put(searchSuccess(data))
            const {errorCode} = result
            if (errorCode == "0") {
                const {isRegister, refreshToken, token} = result.wsResponse
                localStorage.setItem("isRegister", isRegister)
                localStorage.setItem("refreshToken", refreshToken)
                localStorage.setItem("accessToken", token)
                localStorage.setItem("isdn", action.payload.isdn)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess(isRegister, false)
            } if (errorCode == "2") {
                const {isRegister, refreshToken, token} = result.wsResponse
                localStorage.setItem("isRegister", isRegister)
                localStorage.setItem("refreshToken", refreshToken)
                localStorage.setItem("accessToken", token)
                localStorage.setItem("isdn", action.payload.isdn)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess(false, false)
            }if (errorCode == "3") {
                const {isRegister, refreshToken, token} = result.wsResponse
                localStorage.setItem("isRegister", isRegister)
                localStorage.setItem("refreshToken", refreshToken)
                localStorage.setItem("accessToken", token)
                localStorage.setItem("isdn", action.payload.isdn)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess(false, true)
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* otp(action) {
    console.log("======== otp ========", action)
    try {
        const res = yield call((payload) => api.otp(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            // yield put(searchSuccess(data))
            const {errorCode} = result
            if (errorCode == "0") {
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* refresh(action) {
    console.log("======== refresh ========", action)
    try {
        const res = yield call((payload) => api.refreshToken(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
            }
            // yield put(searchSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* register(action) {
    console.log("======== register ========", action)
    try {
        const res = yield call((payload) => api.register(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            // message.success(res.message)
            const {errorCode} = result
            if (errorCode == "0") {
                // if (action.payload.otpType == "1") message.success(result.message)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
        message.error(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* renew(action) {
    console.log("======== renew ========", action)
    try {
        const res = yield call((payload) => api.renew(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res

        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            // message.success(res.message)
            const {errorCode} = result
            if (errorCode == "0") {
                // if (action.payload.otpType == "1") message.success(result.message)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log("ERROR: " + JSON.stringify(e))
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* cancel(action) {
    console.log("======== cancel ========", action)
    try {
        const res = yield call((payload) => api.cancel(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                // message.success(res.message)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(res.message)
            }
            // yield put(searchSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* visitLesson(action) {
    console.log("======== visitLesson ========", action)
    try {
        const res = yield call((payload) => api.visitLesson(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            // message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                // message.success(res.message)
                if (_.isFunction(action.callback)) action.callback()
            } else {
                // message.error(res.message)
            }
            // yield put(searchSuccess(data))
        }
    } catch (e) {
        console.log(e)
    }
}

function* recentLessons(action) {
    console.log("======== recentLessons ========", action)
    try {
        const res = yield call((payload) => api.recentLessons(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                // message.success(res.message)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
                yield put(recentLessonsSuccess(result.wsResponse?.lessons))
            } else {
                message.error(res.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* latestLessons(action) {
    console.log("======== latestLessons ========", action)
    try {
        const res = yield call((payload) => api.latestLessons(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                // message.success(res.message)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
                yield put(latestLessonsSuccess(result.wsResponse?.lessons))
            } else {
                message.error(res.message)
            }
            // yield put(searchSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function *checkRegister(action) {
    console.log("======== checkRegister ========", action)
    try {
        const res = yield call((payload) => api.checkRegister(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                localStorage.setItem("isRegister", true)
                if (_.isFunction(action.callback)) action.callback(true);
            } else if (errorCode == "2") {
                message.error("Você não se registrou para este serviço !")
                setTimeout(() => {
                    localStorage.clear();
                    window.location.href = "/#/login"
                }, 1500)
            } else if (errorCode == "3") {

            }
        }
    } catch (e) {
        console.log(e)
    }
}

function* rootSaga() {
    yield takeLatest(LIST_CATEGORIES, listCategories);
    yield takeLatest(LIST_LESSON_CATEGORIES, listLessonByCategories);
    yield takeLatest(LIST_LESSON_UNIT, listLessonUnits);
    yield takeLatest(LIST_TRAINER, listTrainers);
    yield takeLatest(LIST_BOOKMARK_UNIT, listBookmarkUnit);
    yield takeLatest(BOOKMARK_UNIT, bookmarkUnit);
    yield takeLatest(LOGIN, login);
    yield takeLatest(GET_OTP, otp);
    yield takeLatest(REFRESH_TOKEN, refresh);
    yield takeLatest(REGISTER_SERVICE, register);
    yield takeLatest(CANCEL_SERVICE, cancel);
    yield takeLatest(VISIT_LESSON, visitLesson);
    yield takeLatest(RECENT_LESSONS, recentLessons);
    yield takeLatest(LATEST_LESSONS, latestLessons);
    yield takeLatest(CHECK_REGISTER, checkRegister);
    yield takeLatest(RENEW_SERVICE, renew);
}

export default rootSaga;
