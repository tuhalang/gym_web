import {LIST_LESSON_CATEGORIES_SUCCESS} from "../actions/actions_type";

const initialState = {
    lessons: [],
    categoryName: "Muscle Group",
    total: 0,
}

const lesson = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case LIST_LESSON_CATEGORIES_SUCCESS:
            return {
                ...state,
                lessons: payload.lessons,
                categoryName: payload.category ? payload.category.name : "",
                // total: payload.totalElements,
            }
        default:
            return state
    }
}

export default lesson
