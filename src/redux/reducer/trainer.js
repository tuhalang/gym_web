import {LIST_TRAINER_SUCCESS} from "../actions/actions_type";

const initialState = {
    trainers: [],
    total: 0,
}

const trainer = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case LIST_TRAINER_SUCCESS:
            return {
                ...state,
                trainers: payload.trainers,
                // total: payload.totalElements,
            }
        default:
            return state
    }
}

export default trainer
