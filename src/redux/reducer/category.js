import {LIST_CATEGORIES_SUCCESS} from "../actions/actions_type";

const initialState = {
    categories: [],
    total: 0,
}

const category = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case LIST_CATEGORIES_SUCCESS:
            return {
                ...state,
                categories: payload.categories,
                // total: payload.totalElements,
            }
        default:
            return state
    }
}

export default category
