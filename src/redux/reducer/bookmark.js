import {LIST_BOOKMARK_UNIT_SUCCESS} from "../actions/actions_type";

const initialState = {
    units: [],
    total: 0,
}

const bookmark = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case LIST_BOOKMARK_UNIT_SUCCESS:
            return {
                ...state,
                units: payload.units,
                // total: payload.totalElements,
            }
        default:
            return state
    }
}

export default bookmark
