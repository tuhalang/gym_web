import {combineReducers} from "redux";
import {
    LATEST_LESSONS_SUCCESS,
    RECENT_LESSONS_SUCCESS,
    SHOW_DRAWER,
    SHOW_OTP,
    SHOW_RENEW,
    SHOW_SUBSCRIBER,
    SHOW_VIDEO
} from "../actions/actions_type";
import category from "./category"
import lesson from "./lesson"
import unit from "./unit"
import trainer from "./trainer"
import bookmark from "./bookmark"

const defaultParams = {
    drawer: false,
    subscriber: false,
    isRenew: false,
    video: undefined,
    otp: false,
    recentLessons: [],
    latestLessons: []
};

const common = (state = defaultParams, action) => {
    switch (action.type) {
        case SHOW_DRAWER:
            return {
                ...state,
                drawer: !state.drawer,
            }
        case SHOW_SUBSCRIBER:
            return {
                ...state,
                subscriber: action.visible,
            }
        case SHOW_RENEW:
            return {
                ...state,
                isRenew: action.visible,
            }
        case SHOW_VIDEO:
            return {
                ...state,
                video: action.path
            }
        case SHOW_OTP:
            return {
                ...state,
                otp: action.visible
            }
        case RECENT_LESSONS_SUCCESS:
            return {
                ...state,
                recentLessons: action.payload
            }
        case LATEST_LESSONS_SUCCESS:
            return {
                ...state,
                latestLessons: action.payload
            }
        default:
            return {
                ...state,
            };
    }
}

const allReducers = combineReducers({
    common,
    category,
    lesson,
    unit,
    trainer,
    bookmark,
});

export default allReducers;
