import {LIST_LESSON_UNIT_SUCCESS} from "../actions/actions_type";

const initialState = {
    units: [],
    lesson: {},
    total: 0,
}

const unit = (state, action) => {
    if (typeof state === "undefined") {
        return initialState
    }
    const {payload} = action

    switch (action.type) {
        case LIST_LESSON_UNIT_SUCCESS:
            return {
                ...state,
                units: payload.units,
                lesson: payload.lesson,
                // total: payload.totalElements,
            }
        default:
            return state
    }
}

export default unit
