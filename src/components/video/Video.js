import React from "react";
import {Player} from 'video-react';

import './index.scss'
import {showVideo} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import {CloseCircleOutlined} from "@ant-design/icons";

function Video({width, height}) {

    const dispatch = useDispatch()
    const {video} = useSelector(state => state.common)

    console.log(video)

    return (
        <div className={`video w-full h-100 absolute ${video ? 'flex' : 'none'} justify-center flex-column`}>
            <CloseCircleOutlined className="click close mb-2 mr-2" size={40} onClick={() => dispatch(showVideo(undefined))}/>
            {video ? <Player>
                <source src={video}/>
            </Player> : <div/>}
        </div>
    )
}

export default Video
