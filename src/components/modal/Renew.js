import React from "react";

import './index.scss'
import "../../assets/css/modal_repair.css"
import {useDispatch} from "react-redux";
import {showRenew, register, renew} from "../../redux/actions";
import {Button, Checkbox, message, Modal, Space} from "antd";
import {useHistory} from "react-router-dom";
import {uuidv4} from "../../utils";
import OTPModal from "./Otp";
import {withTranslation} from "react-i18next";

function Renew({t, visible}) {

    const dispatch = useDispatch()
    const history = useHistory()
    const [active, setActive] = React.useState("MOVGYM_MON")
    const [uuid, setUuid] = React.useState('')

    function onClose() {
        dispatch(showRenew(false))
    }

    const [otpVisible, setOtpVisible] = React.useState(false)

    function sendOtp() {
        const _uuid = uuidv4()
        setUuid(_uuid)
        dispatch(renew({
            isdn: localStorage.getItem("isdn"),
            otpType: "0",
            transId: _uuid,
            subService: active,
            callbackSuccess: () => {
                setOtpVisible(true)
            }
        }))
    }

    function subscriber(otp) {
        dispatch(renew({
            isdn: localStorage.getItem("isdn"),
            otpType: "1",
            otp,
            subService: active,
            transId: uuid,
            callbackSuccess: () => {
                setOtpVisible(false)
                dispatch(showRenew(false))
                localStorage.setItem("isRegister", 1)
                message.success("renew success")
                history.push("/")
            }
        }))
    }

    return (
        <Modal
            title={"Renew service"}
            className="subscriber"
            destroyOnClose={true}
            centered
            width={360}
            visible={visible}
            onCancel={onClose}
            footer={[]}
        >
            <div className="flex flex-column">
            
                <Space direction={"vertical"}>
                    <div className="flex-full flex flex-column justify-center">
                        <Button className="w-50 btn mb-5" onClick={sendOtp}>Renovar</Button>
                    </div>
                </Space>
            </div>
            <OTPModal
                visible={otpVisible}
                reSend={() => {
                    sendOtp()
                }}
                handleOk={subscriber}
                title={t('otp-modal-title')}
                onClose={() => setOtpVisible(false)}
            />
        </Modal>
    )
}

export default withTranslation()(Renew)
