import React, {useEffect, useState} from "react";
import {Input} from "antd";
import {useTranslation} from "react-i18next";
import {PRIMARY_COLOR} from "../../utils/constant";

import './index.scss'


function OtpItem({label, placeholder, errorMessage, name, require, reset, value, disabled, autoComplete, reSend}) {
    const [count, setCount] = useState(60);
    const [isReSend, setIsResend] = useState(false);
    const {t} = useTranslation('translation');


    useEffect(() => {
        if (!count) {
            setCount(60)
            setIsResend(true)
        }
        const intervalId = setInterval(() => {
            if (!isReSend) setCount(count - 1);
        }, 1000);

        return () => clearInterval(intervalId);
    }, [count, isReSend])

    return (
        <div className="otp-item">
            {autoComplete ?
                <Input placeholder={placeholder || ''} name={name} className="input-otp" onFocus={reset}
                       defaultValue={value || ''} disabled={disabled} autoComplete={autoComplete}/> :
                <Input placeholder={placeholder || ''} name={name} className="input-otp" onFocus={reset}
                       defaultValue={value || ''} disabled={disabled}/>}
            {isReSend ? <div onClick={() => {
                setIsResend(false)
                reSend()
            }} className="otp-label">{t('get-otp')}</div> : <span className="otp-label">{t('send-otp-back')} <span
                style={{color: PRIMARY_COLOR}}>{count}s</span></span>}
        </div>
    )
}

export default OtpItem;
