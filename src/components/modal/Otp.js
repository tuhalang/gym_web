import React, {useEffect, useState} from "react";
import {Button, Modal} from "antd";
import '../../assets/css/modal_repair.css';
import {useTranslation} from "react-i18next";
import OtpItem from "./OtpItem";
import {PRIMARY_COLOR} from "../../utils/constant";
import './index.scss'
import {showOtp} from "../../redux/actions";


function OTPModal({visible, handleOk, otpError, title, reSend, onClose}) {
    const [loading, setLoading] = useState(false);
    const {t, i18n} = useTranslation('translation');
    const [defaultValue, setDefaultValue] = useState('');


    const onFinish = (event) => {
        event.preventDefault();
        const {otpInput} = event.target;
        if (otpInput) {
            if (otpInput.value === '') {
                return;
            }
            handleOk(otpInput.value)
        }
    };

    const getOtpFromSMS = () => {
        if (!window.OTPCredential) {
            console.log('browser not support')
            return;
        }
        window.addEventListener('DOMContentLoaded', e => {
            const abort = new AbortController();

            setTimeout(() => {
                // abort after two minutes
                abort.abort();
            }, 2 * 60 * 1000);
            navigator.credentials.get({
                signal: abort.signal,
                otp: {
                    transport: ["sms"]
                }
            }).then(({code, type}) => {
                if (code) {
                    setDefaultValue(code)
                }
                abort.abort();
            });
        })
    }

    useEffect(() => {
        getOtpFromSMS()
    }, [])

    return (
        <>
            <Modal title=""
                   destroyOnClose={true}
                   visible={visible}
                   onOk={() => {
                   }}
                   onCancel={onClose}
                   width={300}
                   closable={false}
                   bodyStyle={{borderRadius: 10}}
                   footer={[
                       <form
                           name="otp"
                           onSubmit={onFinish}
                           className="flex flex-column items-center">
                           <OtpItem label={'Otp'} require={true} placeholder={t('otp-modal-placeholder')}
                                    errorMessage={otpError}
                                    name={'otpInput'}
                                    autoComplete={'one-time-code'}
                                    defaultValue={defaultValue}
                                    reSend={reSend}/>
                           <Button style={{backgroundColor: PRIMARY_COLOR}} className="btn"
                                   type={'submit'}
                                   loading={loading}
                                   htmlType="submit">{t('label-confirm-otp')}</Button>
                       </form>
                   ]}
            >
                <div style={{textAlign: 'center',}}>
                    <span className="title">{title || ''}</span>
                </div>
            </Modal>
        </>
    )
}

export default OTPModal;
