import React from "react";

import './index.scss'
import "../../assets/css/modal_repair.css"
import {useDispatch} from "react-redux";
import {showSubscriber, register} from "../../redux/actions";
import {Button, Checkbox, message, Modal, Space} from "antd";
import {useHistory} from "react-router-dom";
import {uuidv4} from "../../utils";
import OTPModal from "./Otp";
import {withTranslation} from "react-i18next";

function Subscriber({t, visible}) {

    const dispatch = useDispatch()
    const [active, setActive] = React.useState("MOVGYM_MON")
    const ref = React.useRef()
    const history = useHistory()
    const contentRef = React.useRef(null);
    const [uuid, setUuid] = React.useState('')
    const [sub, setSub] = React.useState(true)

    function onClose() {
        dispatch(showSubscriber(false))
    }

    const [otpVisible, setOtpVisible] = React.useState(false)

    function sendOtp() {
        const _uuid = uuidv4()
        setUuid(_uuid)
        dispatch(register({
            isdn: localStorage.getItem("isdn"),
            otpType: "0",
            transId: _uuid,
            subService: active,
            callbackSuccess: () => {
                setOtpVisible(true)
            }
        }))
    }

    function subscriber(otp) {
        dispatch(register({
            isdn: localStorage.getItem("isdn"),
            otpType: "1",
            otp,
            subService: active,
            transId: uuid,
            callbackSuccess: () => {
                setOtpVisible(false)
                localStorage.clear()
                dispatch(showSubscriber(false))
                message.success("Subscribe success")
            }
        }))
    }

    function onChange(e) {
        setSub(!e.target.checked);
      }

    return (
        <Modal
            title={"Subscriber to view"}
            className="subscriber"
            destroyOnClose={true}
            centered
            width={360}
            visible={visible}
            onCancel={onClose}
            footer={[]}
        >
            <div className="flex flex-column">
            
                <Space direction={"vertical"}>
                    <Button className={`w-full mt-4 btn ${active === "MOVGYM_DAY" ? "active" : "inactive"}`}
                            onClick={() => setActive("MOVGYM_DAY")}>Register Daily
                        Package (5MT)</Button>
                    <Button className={`w-full mt-4 btn ${active === "MOVGYM_WEEK" ? "active" : "inactive"}`}
                            onClick={() => setActive("MOVGYM_WEEK")}>Register Weekly
                        Package (25MT)</Button>
                    <Button className={`w-full mt-4 btn ${active === "MOVGYM_MON" ? "active" : "inactive"}`}
                            onClick={() => setActive("MOVGYM_MON")}>Register Monthly
                        Package (100MT)</Button>
                    <div className="flex-full flex flex-column justify-center">
                        <Button disabled={sub} className="w-50 btn mb-5" onClick={sendOtp}>Subscriber</Button>
                    </div>
                </Space>
                <Checkbox
                    onChange={onChange}
                    className="checkbox"
                >
                    {'By subscribing you agree to our  Terms & Conditions. You will be charged on the pack chosen. You can cancel the service anytime by sending a message with OFF to 1575 or Calling to 100'}
                </Checkbox>
                {/* <span
                    className="p-2 text-info">{'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique eros consequat dolor maximus.'}</span> */}
            </div>
            <OTPModal
                visible={otpVisible}
                reSend={() => {
                    sendOtp()
                }}
                handleOk={subscriber}
                title={t('otp-modal-title')}
                onClose={() => setOtpVisible(false)}
            />
        </Modal>
    )
}

export default withTranslation()(Subscriber)
