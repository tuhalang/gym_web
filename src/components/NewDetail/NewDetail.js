import React from "react";
import img from "../../assets/images/main-img.png";
import play from "../../assets/images/play-img.png";
import './index.scss'

function NewDetail({}) {
    return (
        <div className="w-full flex flex-column ">
            <span
                className="title">{'Italy won’t face anything like Spai in the Euro 2020 final. Italy won’t face anything like Spai'}</span>
            <span className="time mt-2 mb-2">{'Vừa xong 08/07/2021'}</span>
            <div className="thumbnail-video w-full">
                <img className="main-img" src={img}/>
                <div className="flex items-center justify-center w-full play-img h-100">
                    <img src={play}/>
                </div>
            </div>
            <span className="caption-img mt-1 mb-2">{'Nguồn: Nguyễn Nam'}</span>
            <span
                className='desc'>{'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique eros consequat dolor maximus, condimentum placerat lectus pretium. Integer porttitor rutrum ante, ac auctor dui imperdiet eget.\n' +
            '\n' +
            'Sed blandit laoreet elit ut dapibus. Sed tempor lorem magna, ac viverra sem consectetur quis. Praesent placerat, ipsum vitae placerat mollis, ante justo lacinia ante, sit amet dapibus elit lorem a turpis. Sed elementum dui a sodales placerat. Nunc risus turpis, molestie ut lobortis vel, volutpat nec leo.'}</span>
        </div>
    )
}

export default NewDetail
