import React, {useState} from "react"
import {Drawer, Menu} from "antd";
import {isMobile} from "react-device-detect";

import './index.scss'

import Header from "./header/Header";
import {useDispatch, useSelector} from "react-redux";
import {showDrawer} from "../../redux/actions";
import {LeftOutlined} from "@ant-design/icons";
import {PRIMARY_COLOR} from "../../utils/constant";
import Subscriber from "../modal/Subscriber";
import OTPModal from "../modal/Otp";
import Video from "../video/Video";
import Navigation from "../Navigation";

const menus = [
    // 'Sport news',
    // 'Football',
    // 'Basketball',
    // 'Olympics',
    // 'Formula',
    // 'News',
    // 'Sport Scores',
    // 'Schedule',
    // 'Euro 2020'
]


const Layout = ({children}) => {

    const dispatch = useDispatch()
    const {drawer, subscriber, otp} = useSelector(state => state.common)

    return (
        <div className={`layout flex flex-column absolute items-center`} style={isMobile ? {
            width: '100%'
        } : {}}>
            {/*<Menu onChangeShow={onChangeShow} show={show} />*/}
            <Drawer
                title={<span className="title">Gym</span>}
                placement="left"
                onClose={() => dispatch(showDrawer())}
                closeIcon={<LeftOutlined style={{color: "white"}}/>}
                visible={drawer}
                headerStyle={{
                    backgroundColor: PRIMARY_COLOR,
                }}
                key="drawer"
                className="drawer"
            >
                <Menu>
                    {menus.map(item => <Menu.Item key={item}>{item}</Menu.Item>)}
                </Menu>
            </Drawer>

            <div className="header justify-between items-center">
                <Header/>
            </div>
            <div className="content w-full container">{children}</div>
            <Navigation />
            <Subscriber visible={subscriber}/>
            <OTPModal visible={otp}/>
            <Video/>
        </div>
    )
}

export default Layout
