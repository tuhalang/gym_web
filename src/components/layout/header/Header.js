import React from "react";
import './index.scss'
import uk from "../../../assets/images/GB.png";
import mozambique from "../../../assets/images/mozambique.png";
import {withTranslation} from "react-i18next";
import {PRIMARY_COLOR} from "../../../utils/constant";
import {MenuOutlined} from "@ant-design/icons";
import {useDispatch} from "react-redux";
import {showDrawer} from "../../../redux/actions";
import user from "../../../assets/images/user.png"
import {Space} from "antd";
import {Link, useHistory} from "react-router-dom";
import Dropdown from "../../dropdown/Dropdown";

const listLanguage = [
    {
        name: 'pt',
        image: mozambique
    },
    {
        name: 'en',
        image: uk
    },
];

function Header({t, i18n, icon, onBackPage}) {

    const dispatch = useDispatch()
    const history = useHistory()
    const isLogin = () => localStorage.getItem('accessToken') && localStorage.getItem('accessToken') != '';

    function onAction(id) {
        if (id === "logout") {
            localStorage.clear()
            history.replace("/")
        } 
    }

    React.useEffect(() => {
        i18n.changeLanguage("pt");
    }, []);

    return (
        <div className="header">
            <div className="container">
                <Space className="icon justify-between flex items-center">
                    {/* <img className="user" onClick={() => dispatch(showDrawer())}
                         src={user}/> */}
                    <div className="flex items-center click" onClick={() => history.push(`/bookmark`)}>
                        {isLogin() && <i className='bx bxs-bookmark' style={{
                            fontSize: 25,
                        }}></i>}
                    </div>
                </Space>
                {/*<div className="icon" onClick={onBackPage}>{*/}
                {/*    icon ? <FontAwesomeIcon icon={faChevronLeft} size={11}/> : <div/>*/}
                {/*}</div>*/}
                <span className="title">Mov Gym</span>
                <div className="icon justify-end flex items-center">
                    <div className="icon justify-end flex items-center">
                    {listLanguage.map(e => <img src={e.image} className="flags" style={{
                        backgroundColor: i18n.language == e.name ? 'white' : PRIMARY_COLOR,
                    }} onClick={() => i18n.changeLanguage(e.name)}/>)}
                    </div>

                    <div className="icon justify-end flex items-center">
                    {isLogin() && <Dropdown
                        customToggle={() => <img className="user click" src={user}/>}
                        contentData={[
                            {
                              "icon": "bx bx-log-out-circle bx-rotate-180",
                              "content": "Logout",
                              "id": "logout"
                            }
                        ]}
                        renderItems={(item, index) => {
                            return renderUserMenu(item, index, onAction)}
                        }
                    />}
                    </div>
                </div>
                
            </div>
            <div style={{
                height: 3,
                border: '0.5px solid white',
                backgroundColor: PRIMARY_COLOR,
            }}></div>
            {/*<div className={classes.container2}>*/}
            {/*    <div className={classes.container3}>{isLogin() ?*/}
            {/*        <span onClick={() => {*/}
            {/*            props.logout()*/}
            {/*        }}>{utils.isdn()} <FontAwesomeIcon icon={faSignOutAlt}/> {t('logout')}</span> :*/}
            {/*        <span onClick={() => {*/}
            {/*            props.login()*/}
            {/*        }}><FontAwesomeIcon icon={faSignInAlt}/> {t('login')}</span>*/}
            {/*    }</div>*/}
            {/*    <div>*/}
            {/*        {_.map(listLanguage, e => <img src={e.image} className={classes.flags} style={{*/}
            {/*            backgroundColor: i18n.language == e.name ? 'white' : PRIMARY_COLOR,*/}
            {/*        }} onClick={() => i18n.changeLanguage(e.name)}/>)}*/}
            {/*    </div>*/}
            {/*</div>*/}
        </div>
    )
}

export default withTranslation()(Header);

const renderUserMenu = (item, index, onAction) => (
    <Link to='/' key={index} onClick={() => onAction(item.id)}>
        <div className="notification-item">
            <i className={item.icon}></i>
            <span>{item.content}</span>
        </div>
    </Link>
)
