import React from "react";

import './index.scss'
import {isMobile} from "react-device-detect";
import Header from "./header/Header";

import img from "../../assets/images/login-bg.png"
import img2 from "../../assets/images/login-bg-2.png"
import img3 from "../../assets/images/login-bg-3.png"
import img4 from "../../assets/images/login-bg-4.png"
import {Button, Form, Input, Carousel} from "antd";
import {EyeFilled, LockFilled, PhoneFilled} from "@ant-design/icons";
import {PRIMARY_COLOR} from "../../utils/constant";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {withTranslation} from "react-i18next";
import {login, otp, showSubscriber, showRenew} from "../../redux/actions";
import Subscriber from "../modal/Subscriber";
import Renew from "../modal/Renew";
import OTPModal from "../modal/Otp";

function Login({t, i18n}) {

    const dispatch = useDispatch()
    const history = useHistory()
    const {subscriber, isRenew} = useSelector(state => state.common)
    const [loading, setLoading] = React.useState(false)
    const [visible, setVisible] = React.useState(false)
    const [isdn, setIsdn] = React.useState()

    const onFinish = ({isdn}) => {
        setIsdn(isdn)
        setLoading(true)
        dispatch(otp({
            isdn,
            language: i18n.language,
            callbackSuccess: () => {
                setVisible(true)
            },
            callback: () => {
                setLoading(false)
            }
        }))
    }

    const onLogin = (otp) => {
        setLoading(true)
        dispatch(login({
            isdn,
            otp,
            callback: () => {
                setLoading(false)
            },
            callbackSuccess: (isRegister, isRenew) => {
                if (isRegister === true)
                    history.push("/")
                else {
                    if (isRenew) {
                        setVisible(false)
                        dispatch(showRenew(true))
                    } else {
                        setVisible(false)
                        dispatch(showSubscriber(true))
                    }
                    
                }
            }
        }))
    }

    return (
        <div className={`layout flex flex-column absolute items-center`} style={isMobile ? {
            width: '100%'
        } : {}}>
            <div className="header justify-between items-center">
                <Header/>
            </div>
            <div className="w-full flex-grow flex flex-column">
                <Carousel autoplay>
                    <div><img src={img} className="login-img"/></div>
                    <div><img src={img2} className="login-img"/></div>
                    <div><img src={img3} className="login-img"/></div>
                    <div><img src={img4} className="login-img"/></div>
                </Carousel>
                <div className="login-form flex flex-column items-center flex-grow">
                    <span className="login-label" style={{
                        marginBottom: isMobile ? 18 : 45
                    }}>Login</span>
                    <Form
                        id="login"
                        onFinish={onFinish}
                        footer={[]}
                    >
                        <Form.Item
                            name={"isdn"}
                            rules={[
                                {required: true, message: "Please input your phone"},
                            ]}
                        >
                            <Input prefix={<PhoneFilled/>} type={'tel'} placeholder={'xxxxxxxxx'}/>
                        </Form.Item>
                    </Form>
                    {/*<a>Resend OTP</a>*/}
                    <div className="w-full flex-grow items-center">
                        <Button type={'primary'} className="w-full btn-login" form="login" htmlType={"submit"}
                                loading={loading}>Login</Button>
                    </div>
                    <span className="w-full desc-login">Termos e condições aplicáveis ©2021 Movitel. Todos os direitos reservados. </span>
                </div>
            </div>
            <OTPModal
                visible={visible}
                title={t('otp-modal-title')}
                handleOk={onLogin}
                reSend={() => {
                    onFinish({isdn})
                }}
                onClose={() => setVisible(false)}
            />
            <Subscriber visible={subscriber} />
            <Renew visible={isRenew} />
        </div>
    )
}

export default withTranslation()(Login)
