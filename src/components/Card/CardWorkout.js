import React from "react";

import './index.scss'
import play from '../../assets/images/play-img.png'
import playMini from '../../assets/images/play-mini.png'
import img from '../../assets/images/main-img.png'
import {convertDate} from "../../utils";
import {useHistory} from "react-router";

const convertTitle = (title) => {
    return title?.length > 38 ? title.substring(0, 38) + '...' : title
}

function CardItem({workout}) {

    const history = useHistory()

    return (
        <div className="flex flex-column w-30">
            {workout && workout.imageUrl &&
            <div className="card-item" onClick={() => history.push(`/detail/${workout.id}`)}>
                <img className="main-img" src={workout.imageUrl} style={{
                    height: 60
                }}/>
                <div className="flex items-center justify-center w-full play-img h-100">
                    <img src={playMini}/>
                </div>
            </div>}
            {workout && <span className="desc-item"
                  onClick={() => history.push(`/detail/${workout.id}`)}>{convertTitle(workout.name)}</span>}
        </div>
    )
}

function CardWorkout({onDetail, workout, similarWorkouts}) {

    const history = useHistory()

    const compareJustNow = (time) => {
        let mm = new Date().getTime() - new Date(time)
        if (mm > 0 && mm < 7200000) return "Just now"
        else return ""
    }

    const transformSimilar = () => {
        if (similarWorkouts && similarWorkouts.length < 3) {
            let result = similarWorkouts
            for (let i = similarWorkouts.length - 1; i < 3; i++) {
                result[i] = null;
            }
            return result
        }
        return similarWorkouts
    }

    return (
        <div className="card-workout">
            {workout && workout.imageUrl && <div className="thumbnail-video">
                <img className="main-img" src={workout.imageUrl} style={{
                    height: 180
                }}/>
                <div className="flex items-center justify-center w-full play-img h-100"
                     onClick={() => history.push(`/detail/${workout.id}`)}>
                    <img src={play}/>
                </div>
            </div>}
            <div className="w-full flex mt-2 mb-2">
                <span className="name">{workout.name || ''}</span>
                {/*<span className="time">{`${compareJustNow(workout?.createdAt)} ${convertDate(workout?.createdAt)}`}</span>*/}
            </div>
            {/*<span className="desc" onClick={() => history.push(`/${workout?.categoryId}`)}>{workout?.content}</span>*/}
            <div
                className={`w-full flex justify-between items-center mb-4 mt-3`}>
                {workout && similarWorkouts && transformSimilar().map((item) => <CardItem workout={item}/>)}
            </div>
        </div>
    )
}

export default CardWorkout
