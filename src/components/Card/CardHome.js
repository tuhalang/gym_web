import React from "react";

import './index.scss'
import play from '../../assets/images/play-img.png'
import playMini from '../../assets/images/play-mini.png'
import img from '../../assets/images/main-img.png'

function CardItem({}) {
    return (
        <div className="flex flex-column w-30">
            <div className="card-item">
                <img className="main-img" src={img}/>
                <div className="flex items-center justify-center w-full play-img h-100">
                    <img src={playMini}/>
                </div>
            </div>
            <span className="desc-item">{'Italy won’t face oke anything like ...'}</span>
        </div>
    )
}

function CardHome({onDetail}) {
    return (
        <div className="card">
            <div className="thumbnail-video">
                <img className="main-img" src={img}/>
                <div className="flex items-center justify-center w-full play-img h-100" onClick={() => onDetail(1)}>
                    <img src={play}/>
                </div>
            </div>
            <div className="w-full flex justify-between mt-2 mb-2">
                <span className="name">Football</span>
                <span className="time">{'Vừa xong 08/07/2021'}</span>
            </div>
            <span className="desc">{'Italy won’t face anything like Spai in the Euro 2020 \n' +
            'final. Italy won’t face anything like Spai in the ...'}</span>
            <div className="w-full flex justify-between items-center mb-4 mt-3">
                <CardItem/>
                <CardItem/>
                <CardItem/>
            </div>
        </div>
    )
}

export default CardHome
