import React, {useEffect, useState} from "react";
import _ from "lodash";
import {useTranslation} from "react-i18next";
import {Link, useHistory} from "react-router-dom";
import {ReactComponent as workouts} from '../assets/images/workouts.svg';
import {ReactComponent as trainer} from '../assets/images/trainer.svg';
import {ReactComponent as activity} from '../assets/images/activity.svg';
import {ReactComponent as user} from '../assets/images/user.svg';
import {isMobile} from "react-device-detect";

function Navigation(props) {
    const [active, setActive] = useState("workouts");
    const {t} = useTranslation("translation");
    const history = useHistory()

    const defaultRoutes = [
        {
            icon: workouts,
            type: "image",
            title: t("workouts"),
            id: "workouts",
            enable: true,
        },
        {
            icon: trainer,
            type: "image",
            title: t("trainer"),
            id: "trainer",
            enable: true,
        },
        {
            icon: activity,
            type: "image",
            title: t("categories"),
            id: "categories",
            enable: true,
        },
        {
            icon: user,
            type: "image",
            title: t("profile"),
            id: "profile",
            enable: false,
        },
    ];
    const routes = props.routes || defaultRoutes;

    useEffect(() => {
        if (props.active && props.active != "") setActive(props.active);
    }, [props.active]);

    return (
        <div className="navigation" {...props} style={{
            width: isMobile ? "100%" : 400,
        }}>
            {_.map(routes, (e) => (
                e.enable && <Link
                    key={e.id}
                    className={`item ${active == e.id ? "active" : "inactive"} ${e.enable ? "" : "disabled-link"}`}
                    to={"/" + e.id}
                    onClick={() => {
                        if (e.enable)
                            setActive(e.id);
                    }}
                >

                    <e.icon/>
                    <span
                        className=""
                    >
            {e.title}
          </span>
                </Link>
            ))}
        </div>
    );
}

export default Navigation;
