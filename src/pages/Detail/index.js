import React, {useEffect, useState} from "react";
import "./index.scss"
import workouts from "../../assets/images/workouts.png"
import {Skeleton, Space} from "antd";
import CardDetail from "./_Card";
import {useDispatch, useSelector} from "react-redux";
import {listLessonUnit, visitLesson} from "../../redux/actions";
import {withTranslation} from "react-i18next";
import {useTranslation} from "react-i18next";

function Detail({match: {params: {id}}, i18n}) {

    const dispatch = useDispatch()
    const {t} = useTranslation('translation');
    const [loading, setLoading] = useState(false)
    const {units, lesson} = useSelector(state => state.unit)
    const [pagination, setPagination] = useState({
        page: 0,
        size: 10,
    })

    useEffect(() => {
        setLoading(true)
        dispatch(listLessonUnit({
            ...pagination,
            language: i18n.language,
            lessonId: id,
            callback: () => {
                setLoading(false)
            }
        }))
        dispatch(visitLesson({
            lessonId: id,
            callback: () => {
                console.log("success visit")
            }
        }))
    }, [])

    const {imageUrl, intensity, estimateTime, lessonLevel, name, description} = lesson

    return (
        <Skeleton loading={loading} paragraph={{rows: 15}} className="p-2">
            <div className="detail flex flex-column items-center">
                <div className="banner w-full relative">
                    <img src={imageUrl}/>
                    <div
                        className="info flex flex-column justify-between absolute top-0 left-0 w-full h-100 items-center py-2">
                        <div className="flex flex-column items-center">
                            <span className="title-detail">{name}</span>
                            <span>{description}</span>
                        </div>
                        <div className="flex justify-between w-full p-2">
                            <div className="desc-1 flex flex-column items-center">
                                <span className="content-detail">{estimateTime}</span>
                                <span>{t('avg-minus')}</span>
                            </div>
                            <div className="desc-1 flex flex-column items-center">
                                <span className="content-detail">{intensity}</span>
                                <span>{t('intensity')}</span>
                            </div>
                            <div className="desc-1 flex flex-column items-center">
                                <span className="content-detail">{lessonLevel}</span>
                                <span>{t('level')}</span>
                            </div>
                        </div>
                        {/* <div className="download flex flex-column items-center justify-center">
                            <i className='bx bxs-cloud-download'></i>
                            <span>Download</span>
                        </div> */}
                    </div>
                </div>
                {/* <span className="good-for mt-4">{t('good-for')}</span> */}
                {/* <span className="good-for-1 mb-4 mt-1">{t('good-for-content')}</span> */}
                <div className="list-workouts-title w-full flex justify-start p-2">
                    <span>{t('workouts')}</span>
                </div>
                <Space direction={"vertical"} className="w-full p-2">
                    {units && units.map((item) => <CardDetail title={item.name}
                                                              time={item.estimateTime}
                                                              video={item.videoUrl}
                                                              image={item.imageUrl}
                                                              id={item.id}
                    />)}
                </Space>
            </div>
        </Skeleton>
    )
}

export default withTranslation()(Detail)
