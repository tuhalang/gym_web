import React from "react";

import play from "../../assets/images/play.svg"
import {useDispatch} from "react-redux";
import {bookmarkUnit, showVideo, checkRegister} from "../../redux/actions";
import {message} from "antd";

function CardDetail({video, title, time, image, id}) {

    const dispatch = useDispatch()

    React.useEffect(() => {
        dispatch(checkRegister(
            localStorage.getItem("isdn"),
            localStorage.getItem("token"),
            (isRegister) => {
                console.log("passs")
            }
        ))
    }, [])

    function bookmark() {
        dispatch(bookmarkUnit({
            unitId: id,
            callback: () => {
                // message.success(`Bookmark unit ${title} success`)
            }
        }))
    }

    return (
        <div className="card-detail flex justify-start">
            <div className="play-bounder relative" onClick={() => dispatch(showVideo(video))}>
                <img src={image} className="background"/>
                <div className="flex justify-center items-center absolute top-0 left-0 w-full h-100 bg-gray-1-4">
                    <img src={play}/>
                </div>
            </div>
            <div className="flex flex-column justify-center pl-2 flex-grow">
                <span className="title">{title}</span>
                <span className="time">{time} min</span>
            </div>
            <div className="click" onClick={bookmark}>
                <i className='bx bxs-bookmark'></i>
            </div>
        </div>
    )
}

export default CardDetail
