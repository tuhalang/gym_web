import React from "react";
import {useHistory} from "react-router-dom";

function CardWorkouts({image, title, id}) {

    const history = useHistory()

    return (
        <div className="card relative" onClick={() => history.push(`/category/${id}/0`)}>
            <img src={image}/>
            <span className="title">{title}</span>
        </div>
    )
}

export default CardWorkouts
