import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";

import './index.scss'
import CardWorkouts from "./_Card";
import {Pagination, Skeleton, Space} from "antd";
import {listCategories} from "../../redux/actions";
import {withTranslation} from "react-i18next";


function Categories({i18n}) {

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    const {categories} = useSelector(state => state.category)
    const [pagination, setPagination] = useState({
        page: 0,
        size: 10,
    })

    useEffect(() => {
        setLoading(true)
        if(localStorage.getItem("trainerId") !== undefined && localStorage.getItem("trainerId") !== "" && localStorage.getItem("trainerId") !== null){
            dispatch(listCategories({
                ...pagination,
                language: i18n.language,
                trainerId: localStorage.getItem("trainerId"),
                callback: () => {
                    setLoading(false)
                }
            }))
        }else{
            dispatch(listCategories({
                ...pagination,
                language: i18n.language,
                callback: () => {
                    setLoading(false)
                }
            }))
        }
    }, [])

    return (
        <Skeleton loading={loading} paragraph={{rows: 10}} className="p-2">
            <Space className="workouts flex flex-column w-full items-center mb-4" direction={"vertical"}>
                {categories && categories.map(category => <CardWorkouts image={category.imageUrl} title={category.name}
                                                                        id={category.id}/>)}
            </Space>
        </Skeleton>
    )
}

export default withTranslation()(Categories)
