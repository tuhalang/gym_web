import React from "react";

import './index.scss'
import CardHome from "../../components/Card/CardHome";
import {useDispatch, useSelector} from "react-redux";
// import Lottie from 'react-lottie';
// import animationData from "../../assets/69867-4-dots.json"
import {Skeleton} from "antd";
import {withTranslation} from "react-i18next";
//
// const defaultOptions = {
//     loop: true,
//     autoplay: true,
//     animationData: animationData,
//     rendererSettings: {
//         preserveAspectRatio: 'xMidYMid slice'
//     }
// };

function Workouts({loading, detail, setDetail}) {

    const dispatch = useDispatch();
    const {news} = useSelector(state => state.common)


    return (
        <Skeleton loading={loading} className="pane flex flex-column w-full">
            <span className="label">{'Latest From Editoria'}</span>
            {news && news.length > 0 ? news.map((item) => <CardHome key={news.id} onDetail={setDetail} news={item}/>) :
                <span className="self-center">{'There are no posts'}</span>}
            {/*{loading && <div className="absolute h-100" style={{width: "95%"}}>*/}
            {/*    <Lottie options={defaultOptions}*/}
            {/*            height={150}*/}
            {/*            width={150}/>*/}
            {/*</div>}*/}
        </Skeleton>
    )
}

export default withTranslation()(Workouts)
