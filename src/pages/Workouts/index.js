import React, {useEffect, useState} from "react";

import './index.scss'
// import CardHome from "../../components/Card/CardHome";
import {useDispatch, useSelector} from "react-redux";
// import Lottie from 'react-lottie';
// import animationData from "../../assets/69867-4-dots.json"
import {Skeleton} from "antd";
import CardWorkout from "../../components/Card/CardWorkout";
import {withTranslation} from "react-i18next";
import {latestLessons, recentLessons} from "../../redux/actions";
import _ from "lodash"
import {useTranslation} from "react-i18next";

function Workouts({}) {
    const dispatch = useDispatch();
    const common = useSelector(state => state.common)
    const [loading, setLoading] = useState(false)
    const {t} = useTranslation('translation');

    useEffect(() => {
        setLoading(true)
        dispatch(recentLessons({
            callbackSuccess: () => setLoading(false)
        }))
        dispatch(latestLessons({
            callbackSuccess: () => setLoading(false)
        }))
    }, [])

    function setDetail() {
    }

    return (
        <Skeleton loading={loading}>
            <div className="pane flex flex-column w-full p-1">
                {common.recentLessons && common.recentLessons.length > 0 && <span className="label"><b>{t('recently')}</b></span>}
                {common.recentLessons && common.recentLessons.length > 0 &&
                <CardWorkout onDetail={setDetail} workout={common.recentLessons[0]}
                             similarWorkouts={_.slice(common.recentLessons, 1)}/>}

                {common.latestLessons && common.latestLessons.length > 0 && <span className="label"><b>{t('new-lesson')}</b></span>}
                {common.latestLessons && common.latestLessons.length > 0 ?
                    <CardWorkout onDetail={setDetail} workout={common.latestLessons[0]}
                                 similarWorkouts={_.slice(common.latestLessons, 1)}/> :
                    <span className="self-center">{t('no-lesson')}</span>}
                {/*{loading && <div className="absolute h-100" style={{width: "95%"}}>*/}
                {/*    <Lottie options={defaultOptions}*/}
                {/*            height={150}*/}
                {/*            width={150}/>*/}
                {/*</div>}*/}
            </div>
        </Skeleton>
    )
}

export default withTranslation()(Workouts)