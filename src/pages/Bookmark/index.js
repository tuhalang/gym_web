import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";

import './index.scss'
import {Pagination, Skeleton, Space} from "antd";
import {listBookmarkUnit, listCategories} from "../../redux/actions";
import {withTranslation} from "react-i18next";
import CardBookmark from "./_Card";
import CardDetail from "../Detail/_Card";


function Bookmark({i18n}) {

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    const {units} = useSelector(state => state.bookmark)
    const [pagination, setPagination] = useState({
        page: 0,
        size: 10,
    })

    useEffect(() => {
        setLoading(true)
        dispatch(listBookmarkUnit({
            ...pagination,
            language: i18n.language,
            callback: () => {
                setLoading(false)
            }
        }))
    }, [])

    return (
        <Skeleton loading={loading} paragraph={{rows: 10}} className="p-2">
            <div className="flex items-center flex-column">
                <Space className="workouts flex flex-column w-full items-center mb-4 p-2" direction={"vertical"}>
                    {units && units.map(unit => <CardBookmark title={unit.name}
                                                              time={unit.estimateTime}
                                                              video={unit.videoUrl}
                                                              image={unit.imageUrl}
                                                              id={unit.id}/>)}
                </Space>
                <Pagination {...pagination} size={"small"}/>
            </div>
        </Skeleton>
    )
}

export default withTranslation()(Bookmark)
