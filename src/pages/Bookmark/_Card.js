import React from "react";
import play from "../../assets/images/play.svg"
import {Tooltip} from "antd";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {bookmarkUnit, showVideo} from "../../redux/actions";

function CardBookmark({video, title, desc, time, image, id}) {

    const dispatch = useDispatch()

    function bookmark() {

    }

    return (
        <div className="card-bookmark flex justify-start w-full">
            <div className="play-bounder relative" onClick={() => dispatch(showVideo(video))}>
                <img src={image} className="background"/>
                <div className="flex justify-center items-center absolute top-0 left-0 w-full h-100 bg-gray-1-4">
                    <img src={play}/>
                </div>
            </div>
            <div className="flex flex-column justify-center pl-2 flex-grow">
                <span className="title">{title}</span>
                <span className="time">{time} min</span>
            </div>
            <div className="click" onClick={bookmark}>
                <i className='bx bxs-minus-circle'></i>
            </div>
        </div>
    )
}

export default CardBookmark
