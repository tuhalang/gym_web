import React, {useEffect, useState} from "react";
import "./index.scss"
import body from "../../assets/images/body.png"
import {Button, Empty, Skeleton, Space} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {listCategories, listLessonByCategories, listTrainer} from "../../redux/actions";
import {useHistory} from "react-router-dom";
import CardCategory from "../Category/_Card";
import {withTranslation} from "react-i18next";
import {useTranslation} from "react-i18next";

function TrainerDetail({match: {params: {id}}}, i18n) {

    const {t} = useTranslation('translation');

    const history = useHistory()
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    const {trainers} = useSelector(state => state.trainer)
    const [loadingV2, setLoadingV2] = useState(false)
    const {lessons, categoryName} = useSelector(state => state.lesson)
    const [pagination, setPagination] = useState({
        page: 0,
        size: 1,
        name: "",
        trainerId: id
    })

    const [pagination2, setPagination2] = useState({
        page: 0,
        size: 10,
    })

    useEffect(() => {
        setLoading(true)
        dispatch(listTrainer({
            ...pagination,
            callback: () => setLoading(false)
        }))
    }, [pagination])

    useEffect(() => {
        setLoading(true)
        dispatch(listLessonByCategories({
            ...pagination2,
            trainerId: id,
            callback: () => setLoading(false)
        }))
    }, [pagination2, id])

    const {description, imageUrl, name} = trainers[0] || {}

    return (
        <Skeleton loading={loading} paragraph={{rows: 10}} className="p-2">
            <div className="trainer-detail flex flex-column">
                <img src={imageUrl} className="trainer-img"/>
                <Space className="p-3 flex flex-column items-start" direction={"vertical"}>
                    <span>{name}</span>
                    <span className={"head"}>{t('about')}</span>
                    <span className="content-trainer-detail">{description}</span>
                </Space>
                <Skeleton loading={loadingV2} paragraph={{rows: 10}} className="p-1">
                    <Space direction={"vertical"} className="w-full p-1">
                        {lessons && lessons.length > 0 ? lessons.map(lesson => <CardCategory abstract={lesson.intensity}
                                                                                             title={lesson.name}
                                                                                             desc={lesson.description}
                                                                                             time={lesson.estimateTime || 0}
                                                                                             image={lesson.imageUrl}
                                                                                             id={lesson.id}
                        />) : <Empty/>}
                    </Space>
                </Skeleton>
            </div>
        </Skeleton>
    )
}

export default withTranslation()(TrainerDetail)
