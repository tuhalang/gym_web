import React, {useEffect, useState} from "react";
import "./index.scss"
import CardTrainer from "./_Card";
import {Divider, Input, Skeleton} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {listTrainer} from "../../redux/actions";

function transformDesc(desc) {
    return desc.length > 120 ? desc.substring(0, 120) + "..." : desc
}

function Trainer() {

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false)
    const [value, setValue] = useState('');
    const {trainers} = useSelector(state => state.trainer)
    const [pagination, setPagination] = useState({
        page: 0,
        size: 10,
        name: "",
    })

    const onSearch = (searchText) => {

    };

    useEffect(() => {
        setLoading(true)
        dispatch(listTrainer({
            ...pagination,
            callback: () => setLoading(false)
        }))
    }, [pagination])

    return (
        <div className="trainer flex flex-column items-center p-2">
            <Input prefix={<i className='bx bx-search-alt'></i>} placeholder={"Search"} allowClear={true}
                   className="mb-4 search"
                   onChange={e => setPagination({
                       ...pagination,
                       name: e.target.value,
                   })}
            />
            <Skeleton loading={loading} paragraph={{rows: 10}} className="m-2">
                {
                    trainers && trainers.map(({description, imageUrl, name, id}) => <>
                        <CardTrainer title={name}
                                     desc={transformDesc(description)}
                                     id={id}
                                     image={imageUrl}
                                     time={""}/>
                        <Divider/>
                    </>)
                }
            </Skeleton>
        </div>
    )
}

export default Trainer
