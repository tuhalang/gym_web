import React from "react";

import play from "../../assets/images/play.svg"
import {useHistory} from "react-router-dom";

function CardTrainer({title, desc, time, image, id}) {

    const history = useHistory()

    return (
        <div className="card-trainer flex justify-start">
            <div className="play-bounder relative" onClick={() => history.push(`/trainer/detail/${id}`)}>
                <img src={image} className="background"/>
                <div className="flex justify-center items-center absolute top-0 left-0 w-full h-100 bg-gray-1-4">
                    {/* <img src={play}/> */}
                </div>
            </div>
            <div className="flex flex-column justify-between pl-2 flex-grow">
                <div className="flex justify-between w-full">
                    <span className="title">{title}</span>
                    <div className="flex items-center">
                        {/*<span className="time">{time} min</span>*/}
                        <i className='next bx bx-chevron-right' style={{
                            fontSize: 15,
                        }} onClick={() => history.push(`/trainer/detail/${id}`)}></i>
                    </div>
                </div>
                <span className="desc flex-grow mt-1">{desc}</span>
            </div>
        </div>
    )
}

export default CardTrainer
