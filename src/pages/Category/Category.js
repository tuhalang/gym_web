import React, {useEffect, useState} from "react";

import './index.scss'
import CardCategory from "./_Card";
import {Empty, Skeleton, Space} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {listCategories, listLessonByCategories, listTrainer} from "../../redux/actions";
import {withTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import Filter from "./_Filter";

function Category({match: {params: {id, type}}, i18n}) {

    const dispatch = useDispatch()
    const history = useHistory()
    const [loading, setLoading] = useState(false)
    const [loadingV2, setLoadingV2] = useState(false)
    const {lessons, categoryName} = useSelector(state => state.lesson)
    const {categories} = useSelector(state => state.category)
    const {trainers} = useSelector(state => state.trainer)
    const [pagination, setPagination] = useState({
        page: 0,
        size: 10,
    })

    useEffect(() => {
        if(type == 0){
            dispatch(listCategories({
                page: 0,
                size: 50,
                language: i18n.language,
                parentId: id,
                callback: () => {
                }
            }))
        }
    }, [id])

    useEffect(() => {
        if(type == 0){
            if (categories && categories.length > 0) {
                setLoading(true)
                dispatch(listLessonByCategories({
                    ...pagination,
                    categoryId: categories[0].id,
                    parentId: id,
                    trainerId: "",
                    callback: () => setLoading(false)
                }))
            }
        }else{
            if (trainers && trainers.length > 0) {
                setLoading(true)
                dispatch(listLessonByCategories({
                    ...pagination,
                    trainerId: id,
                    callback: () => setLoading(false)
                }))
                dispatch(listTrainer({
                    ...pagination,
                    trainerId: id,
                    callback: () => {
                        setLoading(false)
                    }
                }))
            }
        }
            
    }, [pagination, categories])

    

    return (
        <Skeleton loading={loading} paragraph={{rows: 15}} className="p-2">
            <div className="flex flex-column p-2">
            <span style={{
                fontSize: 16,
                lineHeight: "20px",
                fontWeight: 600,
                marginBottom: 6,
            }}>{type == 1 ? trainers[0].name : categoryName}</span>
                
                {type == 0 && categories && <Filter data={categories} onChange={(activeKey) => {
                    setLoadingV2(true)
                    dispatch(listLessonByCategories({
                        ...pagination,
                        categoryId: activeKey,
                        parentId: id,
                        trainerId: "",
                        callback: () => setLoadingV2(false)
                    }))
                }}/>}
                <Skeleton loading={loadingV2} paragraph={{rows: 10}}>
                    <Space direction={"vertical"} className="w-full">
                        {lessons && lessons.length > 0 ? lessons.map(lesson => <CardCategory abstract={lesson.intensity}
                                                                                             title={lesson.name}
                                                                                             desc={lesson.description}
                                                                                             time={lesson.estimateTime || 0}
                                                                                             image={lesson.imageUrl}
                                                                                             id={lesson.id}
                        />) : <Empty/>}
                    </Space>
                </Skeleton>
            </div>
        </Skeleton>
    )
}

export default withTranslation()(Category)
