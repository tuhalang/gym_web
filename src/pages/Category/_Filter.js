import React from "react";
import {Tabs} from "antd";

const {TabPane} = Tabs;

function Filter({data, onChange}) {

    return (
        <div className="filter">
            <Tabs size={"small"} onChange={onChange} defaultActiveKey="-1" centered>
                {data.map((item) => <TabPane key={item.id} tab={item.name}></TabPane>)}
            </Tabs>
        </div>
    )
}

export default Filter;
