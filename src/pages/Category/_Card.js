import React from "react";
import play from "../../assets/images/play.svg"
import {Tooltip} from "antd";
import {useHistory} from "react-router-dom";

function CardCategory({abstract, title, desc, time, image, id}) {

    const history = useHistory()

    return (
        <div className="card-category flex justify-start">
            <div className="play-bounder relative" onClick={() => history.push(`/detail/${id}`)}>
                <img src={image} className="background"/>
                <div className="flex justify-center items-center absolute top-0 left-0 w-full h-100 bg-gray-1-4">
                    <img src={play}/>
                </div>
            </div>
            <div className="flex flex-column justify-between pl-2">
                <span className="abstract">{abstract}</span>
                <span className="title">{title}</span>
                <Tooltip title={desc}>
                    <span className="desc">{desc}</span>
                </Tooltip>
                <span className="time">{time} min</span>
            </div>
        </div>
    )
}

export default CardCategory
