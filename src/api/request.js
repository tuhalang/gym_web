import axios from "axios"
import createAuthRefreshInterceptor from "axios-auth-refresh"
import {notification} from "antd"
import {store} from "../index"
import {BASE_URL, GAME_CODE} from "../utils/constant";

const http = require("http")
const https = require("https")

function getAccessToken() {
    return localStorage.getItem("accessToken")
}

function getRefreshToken() {
    return localStorage.getItem("refreshToken")
}

function getLang() {
    return localStorage.getItem("i18nextLng")
}

function getIsdn() {
    return localStorage.getItem("isdn")
}

const refreshAuthLogic = (failedRequest) =>
    axios
        .post(`${BASE_URL}`, {
            isWrap: "1",
            wsCode: "wsRefreshTokenNewsService",
            wsRequest: {
                refreshToken: getRefreshToken(),
                language: getLang(),
                gameCode: GAME_CODE,
                isdn: getIsdn(),
            }
        })
        .then((tokenRefreshResponse) => {
            localStorage.setItem("accessToken", tokenRefreshResponse.data.token)

            failedRequest.response.config.headers["Authorization"] =
                "Bearer " + tokenRefreshResponse.data.token
            return Promise.resolve()
        })
        .catch(() => {
            localStorage.clear()
            // axios.delete("/auth/register", {token: getRefreshToken()})
            window.location.href = "/#/login"
        })

export default function getInstanceAxios(baseAPI) {
    const instance = axios.create({
        baseURL: baseAPI,
        httpAgent: new http.Agent({keepAlive: true}),
        httpsAgent: new https.Agent({keepAlive: true}),
    })

    createAuthRefreshInterceptor(instance, refreshAuthLogic)

    instance.interceptors.request.use(
        function (config) {
            config.headers = {
                // Accept: "application/json",
                // "Content-Type": "application/json",
                // Authorization: getAccessToken()
                //     ? `Bearer ${getAccessToken()}`
                //     : undefined,
                // "Accept-language": localStorage.getItem("locale")
                //     ? localStorage.getItem("locale")
                //     : "vi",
            }
            return config
        },
        function (error) {
            return Promise.reject(error)
        }
    )

    instance.interceptors.response.use(
        function (response) {
            try {
                if (response.data.errorCode === "S200"){
                    if(response.data.result.errorCode === "0" || response.data.result.errorCode === "1" || response.data.result.errorCode === "3" || response.data.result.errorCode === "2"){
                        return response.data
                    }
                    // else{
                    //     if(response.data.result.errorCode === "2"){
                    //         localStorage.clear()
                    //         window.location.href = "/#/login"
                    //         return Promise.reject(response.data.result.message)
                    //     }
                    // }
                }
                return Promise.reject(response.data)
            } catch (error) {
                return Promise.reject(error)
            }
        },
        async function (error) {
            if (error.response) {
                if (error.response.status === 401 || error.response.status === 403) {
                    localStorage.clear()
                    window.location.href = "/#/login"
                    return Promise.reject(error)
                }
                const {response} = error
                const data = response.data
                if (data.message && response.config.method !== "get") {
                    if (data.details && data.details.length > 0) {
                        notification.error({
                            message: data.details[0].msg,
                        })
                    } else {
                        notification.error({
                            message: data.message,
                        })
                    }
                }
            }
            return Promise.reject(error)
        }
    )

    createAuthRefreshInterceptor(instance, refreshAuthLogic)
    return instance
}
