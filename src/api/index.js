import CommonRepository from "./repository/CommonRepository"

const repositories = {
  common: CommonRepository,
}

export default function getFactory(name) {
  return repositories[name]
}
