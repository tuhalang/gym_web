import Client from "../client/Client"
import axios from "axios";

const transformData = ({data, code}) => {
    return {
        isWrap: "0",
        wsCode: code,
        // wsCode: 'wsGetGymCategories',
        wsRequest: {
            wsCode: code,
            isdn: localStorage.getItem("isdn"),
            token: localStorage.getItem("accessToken"),
            wsRequest: {
                ...data,
                language: localStorage.getItem("i18nextLng")
            }
        },
    }
}

const transformData2 = ({data, code}) => {
    return {
        isWrap: "1",
        wsCode: code,
        wsRequest: {
            ...data,
        },
    }
}

const listCategories = (data) => {
    return Client.post(``, transformData({data, code: "wsGetGymCategories"}))
}

const listLessonByCategory = (data) => {
    return Client.post(``, transformData({data, code: "wsGetLessonByCategory"}))
}

const listLessonUnits = (data) => {
    return Client.post(``, transformData({data, code: "wsGetLessonUnits"}))
}

const bookmarkUnit = (data) => {
    return Client.post(``, transformData({data, code: "wsBookmarkUnits"}))
}

const listBookmarkUnit = (data) => {
    return Client.post(``, transformData({data, code: "wsGetBookmarkUnits"}))
}

const login = (data) => {
    return Client.post(``, transformData2({data, code: "wsLoginOtpGymService"}))
}

const otp = (data) => {
    return Client.post(``, ({
        isWrap: "1",
        wsRequest: data,
        wsCode: "wsGetOtpGymService"
        // wsCode: "wsGetOtp",
    }))
}

const refreshToken = (data) => {
    return axios.post(``, transformData({data, code: "wsRefreshTokenGymService"}))
}

const register = (data) => {
    return Client.post(``, transformData2({data, code: "wsRegisterGymService"}))
}

const renew = (data) => {
    return Client.post(``, transformData2({data, code: "wsRenewGymService"}))
}

const cancel = (data) => {
    return Client.post(``, transformData({data, code: "wsCancelGymService"}))
}

const listTrainers = (data) => {
    return Client.post(``, transformData({data, code: "wsGetTrainers"}))
}

const visitLesson = (data) => {
    return Client.post(``, transformData({data, code: "wsVisitedLesson"}))
}

const recentLessons = (data) => {
    return Client.post(``, transformData({data, code: "wsGetRecentLessons"}))
}

const latestLessons = (data) => {
    return Client.post(``, transformData({data, code: "wsGetLatestLessons"}))
}

const checkRegister = (data) => {
    return Client.post(``, ({
        isWrap: "1",
        wsRequest: data,
        wsCode: "wsCheckSubRegiser"
    }))
}


const commonRepository = {
    login,
    otp,
    refreshToken,
    register,
    cancel,
    listCategories,
    listLessonByCategory,
    listLessonUnits,
    bookmarkUnit,
    listBookmarkUnit,
    listTrainers,
    visitLesson,
    recentLessons,
    latestLessons,
    checkRegister,
    renew
}
export default commonRepository
