import React, {Suspense, useEffect, Fragment} from "react"
import {
    Route,
    Switch,
    Redirect,
    Link, HashRouter,
} from "react-router-dom"

import {Result} from "antd"
import Layout from "./components/layout/Layout";
import {useDispatch} from "react-redux";
import SuspenseComponent from "./components/router/SuspenseComponent";
import Login from "./components/layout/Login";
import Categories from "./pages/Categories";
import Category from "./pages/Category/Category";
import Detail from "./pages/Detail";
import Trainer from "./pages/Trainer";
import TrainerDetail from "./pages/Trainer/Detail";
import Bookmark from "./pages/Bookmark";
import Workouts from "./pages/Workouts"

function PrivateRoute({children, ...rest}) {
    const checkToken = localStorage.getItem("accessToken")
    // const checkToken = true
    return (
        <Route
            {...rest}
            render={() => (checkToken ? children : <Redirect to="/login"/>)}
        />
    )
}

const ProtectedPage = () => {
    const dispatch = useDispatch()

    function WaitingComponent(Component) {
        return (props) => (
            <Suspense fallback={<SuspenseComponent/>}>
                <Component {...props} />
            </Suspense>
        )
    }

    useEffect(() => {
    }, [])

    return (
        <Fragment>
            <Layout>
                <Switch>
                    {/*<Route*/}
                    {/*    path="/shipping-orders/upload"*/}
                    {/*    exact*/}
                    {/*    component={WaitingComponent(*/}
                    {/*        OrderUploadShipping,*/}
                    {/*        "SHIPPING_ORDER"*/}
                    {/*    )}*/}
                    {/*/>*/}
                    <Route path="/bookmark" exact component={WaitingComponent(Bookmark)}/>
                    <Route path="/trainer" exact component={WaitingComponent(Trainer)}/>
                    <Route path="/trainer/detail/:id" exact component={WaitingComponent(TrainerDetail)}/>
                    <Route path="/categories" exact component={WaitingComponent(Categories)}/>
                    <Route path="/category/:id/:type" exact component={WaitingComponent(Category)}/>
                    <Route path="/detail/:id" exact component={WaitingComponent(Detail)}/>
                    <Route path="/workouts" exact component={WaitingComponent(Workouts)}/>
                    <Route path="/" exact component={WaitingComponent(Workouts)}/>
                    <Route component={NotLoadAuthorization}/>
                </Switch>
            </Layout>
        </Fragment>
    )
}

const Routes = () => {
    return (
        <HashRouter>
            <Switch>
                <Route exact path="/login" component={Login}/>
                <PrivateRoute path="/">
                    <ProtectedPage/>
                </PrivateRoute>
            </Switch>
        </HashRouter>
    )
}

export default Routes

const NotLoadAuthorization = () => {
    return (
        <Result
            status="404"
            title="404"
            subTitle="Trang không tồn tại"
            extra={<Link to="/">Trở về trang chủ</Link>}
        />
    )
}
